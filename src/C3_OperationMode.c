#include "C3_OperationMode.h"

void C3_OP_OpMode_clear(C3_OP_OpMode* opMode) {
    if(opMode == NULL) return;
    free(opMode->name);
}

bool C3_OP_OpMode_copy(const C3_OP_OpMode* src, C3_OP_OpMode* dest) {
    if(dest == NULL)
        return false;

    dest->name = malloc(sizeof(char) * (strlen(src->name)+1));
    if(dest->name == NULL)
        return false;
    strcpy(dest->name, src->name);

    dest->init = src->init;
    dest->execute = src->execute;
    dest->clear = src->clear;
    dest->context = src->context;
    dest->select = src->select;
    dest->deselect = src->deselect;
    return true;
}
