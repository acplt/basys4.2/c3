#include "C3_ControlComponent.h"

typedef struct C3_CC {
    C3_Info info;
    C3_Status status;
    int opMode;
    size_t opModesSize;
    C3_OP_OpMode* opModes;
    C3_IOConfig ioConfig;
    C3_IO io;
} C3_CC;

C3_CC* C3_CC_new() {
    C3_CC* cc = (C3_CC*) calloc(1, sizeof(C3_CC));
    if(cc == NULL)
        return NULL;
    if(!C3_CC_init(cc)){
        free(cc);
        return NULL;
    }    
    return cc;
}

bool C3_CC_init(C3_CC* cc) {
    if(cc == NULL )
        return false;
    
    if(!C3_Info_init(&cc->info))
        return false;

    if(!C3_Status_init(&cc->status)) {
        C3_Info_clear(&cc->info);
        return false;
    }
    C3_IOConfig_init(&cc->ioConfig, &cc->io);
    cc->opMode = C3_OP_OPMODE_NONE_IDX;
    cc->opModes = NULL;
    cc->opModesSize = 0;
    return true;
}

void C3_CC_clear(C3_CC* cc) {
    if(cc == NULL) return;
    if(cc->opModes != NULL) {
        if((cc->opMode != C3_OP_OPMODE_NONE_IDX) &&
            cc->opModes[cc->opMode].deselect != NULL) {
                cc->opModes[cc->opMode].deselect(cc, &cc->opModes[cc->opMode]);
        }
            
        for (size_t i = 0; i < cc->opModesSize; i++)
        {
            if(cc->opModes[i].clear)
                cc->opModes[i].clear(cc, &cc->opModes[i]);
            C3_OP_OpMode_clear(&cc->opModes[i]);
        }
        free(cc->opModes);
    }
    C3_Info_clear(&cc->info);
    C3_Status_clear(&cc->status);
    C3_IOConfig_clear(&cc->ioConfig);
    free(cc->io);
}

bool C3_CC_setInfo(C3_CC* cc, C3_Info* info) {
    C3_Info_clear(&cc->info);
    return C3_Info_copy(info, &cc->info);
}

const C3_Info C3_CC_getInfo(C3_CC* cc) {
    return cc->info;
}

const char* C3_CC_getId(C3_CC* cc) {
    return cc->info.id;
}

const C3_Status C3_CC_getStatus(C3_CC* cc) {
    return cc->status;
}

char* C3_CC_printStatus(C3_CC* cc) {
    return C3_Status_print(&cc->status, ",");
}

void C3_CC_setIOConfig(C3_CC* cc, C3_IOConfig ioConfig) {
    C3_IOConfig_clear(&cc->ioConfig);
    free(cc->io);
    cc->ioConfig = ioConfig;
    C3_IOConfig_init(&cc->ioConfig, &cc->io);
}

C3_Result C3_CC_addOperationMode(C3_CC* cc, C3_OP_OpMode* opMode) {
    if((cc->info.profile.OM == C3_OM_NONE) && (cc->opModesSize > 0)) {
        return C3_RESULT_BAD_PROFILE;
    }
    if(cc->info.profile.OM == C3_OM_CONTI && (cc->opModesSize > 2)) {
        //TODO check names (e.g. "STARTUP", "SHUTDOWN", "BSTATE"/"DEFAULT/CONTI")
        return C3_RESULT_BAD_PROFILE;
    }

    if(cc->opModesSize >= INT_MAX)
        return C3_RESULT_BAD;
    for (size_t i = 0; i < cc->opModesSize; i++)
    {
        if(strcmp(cc->opModes[i].name, opMode->name) == 0)
            return C3_RESULT_BAD_OPMODENAME;
    }

    if(!C3_Status_checkOpModeName(opMode->name)) {
        return C3_RESULT_BAD_OPMODENAME;
    }

    C3_OP_OpMode* newOpModes = (C3_OP_OpMode*) realloc(cc->opModes, (cc->opModesSize + 1) * sizeof(C3_OP_OpMode));
    if(newOpModes == NULL)
        return C3_RESULT_BAD_MEMORY;
    if(!C3_OP_OpMode_copy(opMode, &newOpModes[cc->opModesSize]))
    {
        cc->opModes= realloc(newOpModes, (cc->opModesSize) * sizeof(C3_OP_OpMode));
        return C3_RESULT_BAD_MEMORY;
    }
    if(newOpModes[cc->opModesSize].init && !newOpModes[cc->opModesSize].init(cc, &newOpModes[cc->opModesSize]))
    {
        C3_OP_OpMode_clear(&newOpModes[cc->opModesSize]);
        cc->opModes = realloc(newOpModes, (cc->opModesSize) * sizeof(C3_OP_OpMode));
        return C3_RESULT_BAD;
    }
    cc->opModes = newOpModes;
    cc->opModesSize++;

    if(cc->opModesSize == 1) {
        // Select new operation mode if it is the first one added
        if(cc->opModes->select == NULL ||
            cc->opModes->select(cc, cc->opModes)) {
            cc->opMode = 0;
            cc->status.operationMode = cc->opModes[cc->opMode].name;
        } else {
            return C3_RESULT_BAD_OPMODECHANGE;
        }
    }
    return C3_RESULT_GOOD;
}

C3_Result C3_CC_deleteOperationMode(C3_CC* cc, const char* name) {
    int opModeIndex = -1;
    for (size_t i = 0; i < cc->opModesSize; i++) {
        if(strcmp(cc->opModes[i].name, name) == 0) {
            opModeIndex = i;
            break;
        }
    }
    if(opModeIndex == -1) {
        return C3_RESULT_BAD_OPMODEUNKNOWN;
    }
    else if(opModeIndex == cc->opMode) {
        if(cc->opModes[cc->opMode].deselect == NULL ||
            cc->opModes[cc->opMode].deselect(cc, &cc->opModes[cc->opMode])) {
            cc->opMode = C3_OP_OPMODE_NONE_IDX;
            cc->status.operationMode = C3_OP_OPMODE_NONE;
        } else {
            return C3_RESULT_BAD_OPMODECHANGE;
        }
    }

    if(cc->opModes[opModeIndex].clear)
        cc->opModes[opModeIndex].clear(cc, &cc->opModes[opModeIndex]);
    C3_OP_OpMode_clear(&cc->opModes[opModeIndex]);

    if(cc->opModesSize == 1) {
        free(cc->opModes);
        cc->opModes = NULL;
    } else {
        cc->opModes = (C3_OP_OpMode*) realloc(cc->opModes, (cc->opModesSize) * sizeof(C3_OP_OpMode));
    }
    --cc->opModesSize;
    if(cc->opModesSize == 1) {
        // Select last operation mode on default
        if(cc->opModes->select == NULL ||
            cc->opModes->select(cc, cc->opModes)) {
            cc->opMode = 0;
            cc->status.operationMode = cc->opModes[cc->opMode].name;
        } else {
            return C3_RESULT_BAD_OPMODECHANGE;
        }
    }
    return C3_RESULT_GOOD;
}

size_t C3_CC_getOperationModeNames(C3_CC* cc, char*** modeNames) {
    if(modeNames) {
        char** newModeNames = calloc(cc->opModesSize, sizeof(char*));
        if(newModeNames == NULL)
            return 0;
        for (size_t i = 0; i < cc->opModesSize; i++)
        {
            newModeNames[i] = malloc((strlen(cc->opModes[i].name) + 1) * sizeof(char));
            if(newModeNames[i]==NULL) {
                for (size_t j = 0; j < i; j++)
                {
                    free(newModeNames[j]);
                }
                free(newModeNames);
                return 0;
            }
            strcpy(newModeNames[i], cc->opModes[i].name);
        }
        *modeNames = newModeNames;
    }
    return cc->opModesSize;
} 

void C3_CC_runIterate(C3_CC* cc)
{
    //Update Inputs
    if((cc->info.profile.EM & C3_EM_SIMULATE) && (cc->status.executionMode == C3_EM_STATE_SIMULATE)) {
        if(cc->ioConfig.readSim)
            cc->ioConfig.readSim(cc->ioConfig.context, cc->io);
    }else {
        if(cc->ioConfig.read)
            cc->ioConfig.read(cc->ioConfig.context, cc->io);
    }
    
    // Execute selected operation mode
    if(cc->opMode != C3_OP_OPMODE_NONE_IDX){
        if(cc->opModes[cc->opMode].execute) {
            C3_ES_Order esOrder = -1;
            const char* newWorkingState = NULL;
            const char* newErrorState = NULL;
            cc->opModes[cc->opMode].execute(cc, &cc->opModes[cc->opMode], cc->io,
                                            &esOrder, &newWorkingState, &newErrorState);
            
            if(newWorkingState != NULL) {
                free(cc->status.workingState);
                cc->status.workingState = malloc(sizeof(char) * (strlen(newWorkingState) + 1));
                if(cc->status.workingState != NULL)
                    strcpy(cc->status.workingState, newWorkingState);
            }
            if(newErrorState != NULL) {
                if(strcmp(cc->status.error, C3_ERRORSTATE_NONE) != 0) {
                    if(strcmp(cc->status.lastError, cc->status.error) != 0) {
                        free(cc->status.lastError);
                        cc->status.lastError = malloc(sizeof(char) * (strlen(cc->status.error) + 1));
                        if(cc->status.lastError != NULL) {
                            strcpy(cc->status.lastError, cc->status.error);
                        }
                    }
                }
                free(cc->status.error);
                cc->status.error = malloc(sizeof(char) * (strlen(newErrorState) + 1));
                if(cc->status.error != NULL)
                    strcpy(cc->status.error, newErrorState);
            }
            if(esOrder != -1) {
                C3_ES_executeOrder(cc->info.profile.ES, esOrder, cc->status.executionState, &cc->status.executionState);
            }
        }
    }

    // Execute interlock
    if(cc->ioConfig.interlock)
        cc->ioConfig.interlock(cc->ioConfig.context, cc->io);

    // Update Outputs
    if((cc->info.profile.EM & C3_EM_SIMULATE) && (cc->status.executionMode == C3_EM_STATE_SIMULATE)) {
        if(cc->ioConfig.writeSim)
            cc->ioConfig.writeSim(cc->ioConfig.context, cc->io);
    }else {
        if(cc->ioConfig.write)
            cc->ioConfig.write(cc->ioConfig.context, cc->io);
    }
}

static C3_Result evaluateCmd(C3_CC* cc, C3_Command* cmd, char* cmdStr) {
    // TODO add logger
    //printf("%s recieved command: %s\n", cc->info.id, cmdStr);
    free(cc->status.lastCMD);
    cc->status.lastCMD = cmdStr;

    // TODO Check ORDER in CMDOLIST if CMDOLIST is present
    if(cmd->type == C3_CMDTYPE_UNKNOWN) {
        C3_Command_parseOrder(cmd);
    }

    // Handle occupation change
    if(cmd->type == C3_CMDTYPE_OC) {
        return C3_OC_executeOrder(cc->info.profile.OC, cmd->sender, cmd->order.num, cc->status.occupationState,
            &cc->status.occupationState, &cc->status.occupier, &cc->status.lastOccupier);
    }
    
    // Check occupation if OC facet includes occupation
    if((cc->info.profile.OC > C3_OC_NONE) && 
        strcmp(cmd->sender ? cmd->sender : C3_OC_OCCUPIER_ANONYM, cc->status.occupier) != 0) {
        return C3_RESULT_BAD_ACCESS;
    }

    // Handle operation mode change
    if(cmd->type == C3_CMDTYPE_OP) {
        if(cc->info.profile.OM == C3_OM_NONE)
            return C3_RESULT_BAD_PROFILE;
        if(cc->info.profile.OM == C3_OM_PARALLEL)
            return C3_RESULT_BAD_NOTIMPLEMENTED;

        int newOpModeIdx = -1;
        for (size_t i = 0; i < cc->opModesSize; i++) {
            if(strcmp(cc->opModes[i].name, cmd->order.str) == 0) {
                newOpModeIdx = i;
                break;
            }
        }
        if(newOpModeIdx == -1) {
            return C3_RESULT_BAD_OPMODEUNKNOWN;
        }
        else if(newOpModeIdx == cc->opMode) {
            return C3_RESULT_GOOD_TARGETSTATEACTIVE;
        }
        else {
            // Deselct current operation mode
            if(cc->opMode != C3_OP_OPMODE_NONE_IDX) {
                if(cc->opModes[cc->opMode].deselect == NULL ||
                    cc->opModes[cc->opMode].deselect(cc, &cc->opModes[cc->opMode])) {
                    cc->opMode = C3_OP_OPMODE_NONE_IDX;
                    cc->status.operationMode = C3_OP_OPMODE_NONE;
                } else {
                    return C3_RESULT_BAD_OPMODECHANGE;
                }
            }
            // Select new operation mode
            if(cc->opModes[newOpModeIdx].select == NULL ||
                cc->opModes[newOpModeIdx].select(cc, &cc->opModes[newOpModeIdx])) {
                cc->opMode = newOpModeIdx;
                cc->status.operationMode = cc->opModes[cc->opMode].name;
            } else {
                return C3_RESULT_BAD_OPMODECHANGE;
            }
            return C3_RESULT_GOOD;
        }
    }

    // Handle execution mode change
    if(cmd->type == C3_CMDTYPE_EM) {
        return C3_EM_executeOrder(cc->info.profile.EM, cmd->order.num, cc->status.executionMode, &cc->status.executionMode);
    }

    // Handle execution state change
    if(cmd->type == C3_CMDTYPE_ES) {
        return C3_ES_executeOrder(cc->info.profile.ES, cmd->order.num, cc->status.executionState, &cc->status.executionState);
    }

    return C3_RESULT_BAD_CMD;
}

C3_Result C3_CC_CmdString(C3_CC* cc, const char* cmd) {
    C3_Result result;
    C3_Command* cmdObj = C3_Command_parse(cmd);
    if(cmdObj) {
        char* cmdStrCpy = malloc(sizeof(char) * (strlen(cmd)+1));
        if(cmdStrCpy == NULL) {
            result = C3_RESULT_BAD_MEMORY;
        } else {
            strcpy(cmdStrCpy, cmd);
            result = evaluateCmd(cc, cmdObj, cmdStrCpy);
        }
        C3_Command_clear(cmdObj);
        return result;
    } else {
        result = C3_RESULT_BAD_CMDSYNTAX;
    }
    return result;
}


C3_Result C3_CC_Cmd(C3_CC* cc, C3_Command* cmd) {
    char* cmdStr = C3_Command_print(cmd);
    if(cmdStr) {
        return evaluateCmd(cc, cmd, cmdStr);
    }
    return C3_RESULT_BAD_MEMORY;
}

