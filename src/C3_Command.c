#include "C3_Command.h"

C3_Command* C3_Command_new(void) {
    C3_Command* cmd = calloc(1, sizeof(C3_Command));
    return cmd;
}

void C3_Command_clear(C3_Command* cmd) {
    if(!cmd)
        return;
    if(cmd->sender) {
        free(cmd->sender);
        cmd->sender = NULL;
    }
    if(cmd->type ==  C3_CMDTYPE_OP || cmd->type ==  C3_CMDTYPE_UNKNOWN) {
        free(cmd->order.str);
        cmd->order.str = NULL;
    }
    if(cmd->paramCount > 0) {
        if(cmd->paramKeys) {
            for (size_t i = 0; i < cmd->paramCount; i++)
            {
                if(cmd->paramKeys[i])
                    free(cmd->paramKeys[i]);
            }
            free(cmd->paramKeys);
            cmd->paramKeys = NULL;
        }
        if(cmd->paramVals) {
            for (size_t i = 0; i < cmd->paramCount; i++)
            {
                if(cmd->paramVals[i])
                    free(cmd->paramVals[i]);
            }
            free(cmd->paramVals);
            cmd->paramVals = NULL;
        }
    }
}

static const char* getOrderFromCmd(C3_Command* cmd) {
    switch (cmd->type)
    {
    case C3_CMDTYPE_OC:
        return C3_OC_ORDERSTRING[cmd->order.num];
    case C3_CMDTYPE_EM:
        return C3_EM_ORDERSTRING[cmd->order.num];
    case C3_CMDTYPE_ES:
        return C3_ES_ORDERSTRING[cmd->order.num];
    case C3_CMDTYPE_OP:
    case C3_CMDTYPE_UNKNOWN:
        break;
    }
    return cmd->order.str;
}

char* C3_Command_print(C3_Command* cmd) {
    if(cmd == NULL)
        return NULL;
    int size = strlen(cmd->sender ? cmd->sender : C3_OC_OCCUPIER_ANONYM) + strlen(getOrderFromCmd(cmd)) + 3;
    if(cmd->paramCount > 0) {
        for (size_t i = 0; i < cmd->paramCount; i++)
        {
            size += strlen(cmd->paramKeys[i]) + strlen(cmd->paramVals[i]) + 2;
        }
        size--; // don't print last ","
    }
    
    char* result = malloc(size * sizeof(char));
    if(result == NULL)
        return NULL;
    int ptr = sprintf(result, "%s;%s;", cmd->sender ? cmd->sender : C3_OC_OCCUPIER_ANONYM, getOrderFromCmd(cmd));
    if(cmd->paramCount > 0) {
        ptr += sprintf(result + ptr, "%s=%s", cmd->paramKeys[0], cmd->paramVals[0]);
        for (size_t i = 1; i < cmd->paramCount; i++)
        {
            ptr += sprintf(result + ptr, ",%s=%s", cmd->paramKeys[i], cmd->paramVals[i]);
        }
    }

    return result;
}

void C3_Command_parseOrder(C3_Command* cmd){
    for (size_t i = 0; i < C3_OC_ORDERSIZE; i++)
    {
        if(strcmp(C3_OC_ORDERSTRING[i], cmd->order.str) == 0) {
            cmd->type = C3_CMDTYPE_OC;
            free(cmd->order.str);
            cmd->order.num = i;
            return;
        }
    }
    for (size_t i = 0; i < C3_EM_ORDERSIZE; i++)
    {
        if(strcmp(C3_EM_ORDERSTRING[i], cmd->order.str) == 0) {
            cmd->type = C3_CMDTYPE_EM;
            free(cmd->order.str);
            cmd->order.num = i;
            return;
        }
    }
    for (size_t i = 0; i < C3_ES_ORDERSIZE; i++)
    {
        if(strcmp(C3_ES_ORDERSTRING[i], cmd->order.str) == 0) {
            cmd->type = C3_CMDTYPE_ES;
            free(cmd->order.str);
            cmd->order.num = i;
            return;
        }
    }
    // assume that it is an operation mode
    cmd->type = C3_CMDTYPE_OP;
}

//[sender];[order];[param=value[,param=value]]
// escape character for comma in values: backslash
C3_Command* C3_Command_parse(const char* cmd) {
    int cmdSize = strlen(cmd);
    int senderSize = -1;
    int orderSize = -1;
    for (size_t i = 0; i < cmdSize; i++)
    {
        if(cmd[i] == ';') {
            if(senderSize == -1) {
                senderSize = i;
            } else {
                orderSize = i - senderSize -1;
                break;
            }
        } 
    }
    if(orderSize == -1)
        return NULL;
    C3_Command* result = C3_Command_new();
    if(!result)
        return NULL;
    if(senderSize > 0) {
        result->sender = malloc((senderSize+1) * sizeof(char));
        if(result->sender == NULL)
            goto cleanup;
        strncpy(result->sender, cmd, senderSize);
        result->sender[senderSize] = '\0';
    }
    result->order.str = malloc((orderSize+1) * sizeof(char));
    if(result->order.str == NULL)
        goto cleanup;

    strncpy(result->order.str, cmd + senderSize +1, orderSize);
    result->order.str[orderSize] = '\0';
    C3_Command_parseOrder(result);

    if(cmdSize > senderSize+orderSize+2) {
        result->paramCount++;
        int paramIdx = senderSize+orderSize+2;
        for (size_t i = paramIdx; i < cmdSize; i++)
        {
            if(cmd[i] == ',' && (cmd[i-1] !='\\' || cmd[i-2] == '\\')) {
                result->paramCount++;
            }
        }
        result->paramKeys = calloc(result->paramCount, sizeof(char*));
        result->paramVals = calloc(result->paramCount, sizeof(char*));
        if(result->paramKeys == NULL || result->paramVals == NULL)
            goto cleanup;
        
        for (size_t i = 0; i < result->paramCount; i++)
        {
            size_t keyIdx = paramIdx;
            while (keyIdx < cmdSize && !(cmd[keyIdx] == '='))
            {
                keyIdx++;
            }
            size_t valIdx = keyIdx+1;
            while (valIdx < cmdSize && !(cmd[valIdx] == ',' && (cmd[valIdx-1] !='\\' || cmd[valIdx-2] == '\\')))
            {
                valIdx++;
            }
            valIdx = valIdx == cmdSize ? valIdx+1 : valIdx;
            result->paramKeys[i] = malloc((keyIdx-paramIdx+1) * sizeof(char));
            result->paramVals[i] = malloc((valIdx-keyIdx) * sizeof(char));
            if(result->paramKeys[i] == NULL || result->paramVals[i] == NULL)
                goto cleanup;
            strncpy(result->paramKeys[i], cmd + paramIdx, keyIdx-paramIdx);
            result->paramKeys[i][keyIdx-paramIdx] = '\0';
            strncpy(result->paramVals[i], cmd + keyIdx +1, valIdx-keyIdx-1);
            result->paramVals[i][valIdx-keyIdx-1] = '\0';
            paramIdx = valIdx+1;
        }
    }
    return result;

    cleanup:
        C3_Command_clear(result);
        free(result);
        return NULL;
}