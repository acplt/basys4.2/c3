#include "C3_Status.h"

bool C3_Status_init(C3_Status* status) {
    if(status == NULL)
        return false;
    
    status->lastCMD = malloc(sizeof(char) * 1);
    status->occupier = malloc(sizeof(char) * (strlen(C3_OC_OCCUPIER_FREE)+1));
    status->lastOccupier = malloc(sizeof(char) * (strlen(C3_OC_OCCUPIER_FREE)+1));
    status->workingState = malloc(sizeof(char) * (strlen(C3_WORKSTATE_NONE)+1));
    status->error = malloc(sizeof(char) * (strlen(C3_ERRORSTATE_NONE)+1));
    status->lastError = malloc(sizeof(char) * (strlen(C3_ERRORSTATE_NONE)+1));
    
    if(status->lastCMD == NULL ||
       status->occupier == NULL ||
       status->lastOccupier == NULL ||
       status->workingState == NULL ||
       status->error == NULL ||
       status->lastError == NULL ) {
        C3_Status_clear(status);
        return false;
    }
    
    strcpy(status->lastCMD, "");
    status->occupationState = C3_OC_STATE_FREE;
    strcpy(status->occupier, C3_OC_OCCUPIER_FREE);
    strcpy(status->lastOccupier, C3_OC_OCCUPIER_FREE);
    status->executionMode = C3_EM_STATE_AUTO;
    status->executionState = C3_ES_STATE_STOPPED;
    status->operationMode = C3_OP_OPMODE_NONE;
    strcpy(status->workingState, C3_WORKSTATE_NONE);
    strcpy(status->error, C3_ERRORSTATE_NONE);
    strcpy(status->lastError, C3_ERRORSTATE_NONE);
    return true;
}

void C3_Status_clear(C3_Status* status) {
    if(status == NULL) return;
    free(status->lastCMD);
    free(status->occupier);
    free(status->lastOccupier);
    free(status->workingState);
    free(status->error);
    free(status->lastError);
}

bool C3_Status_copy(const C3_Status* src, C3_Status* dest) {
    if(dest == NULL)
        return false;
    dest->lastCMD = malloc(sizeof(char) * (strlen(src->lastCMD)+1));
    dest->occupier = malloc(sizeof(char) * (strlen(src->occupier)+1));
    dest->lastOccupier = malloc(sizeof(char) * (strlen(src->lastOccupier)+1));
    dest->workingState = malloc(sizeof(char) * (strlen(src->workingState)+1));
    dest->error = malloc(sizeof(char) * (strlen(src->error)+1));
    dest->lastError = malloc(sizeof(char) * (strlen(src->lastError)+1));

    if(dest->lastCMD == NULL ||
       dest->occupier == NULL ||
       dest->lastOccupier == NULL ||
       dest->workingState == NULL ||
       dest->error == NULL ||
       dest->lastError == NULL ) {
        C3_Status_clear(dest);
        return false;
    }

    strcpy(dest->lastCMD, src->lastCMD);
    dest->occupationState = src->occupationState;
    strcpy(dest->occupier, src->occupier);
    strcpy(dest->lastOccupier, src->lastOccupier);
    dest->executionMode = src->executionMode;
    dest->executionState = src->executionState;
    dest->operationMode = src->operationMode;
    strcpy(dest->workingState, src->workingState);
    strcpy(dest->error, src->error);
    strcpy(dest->lastError, src->lastError);

    return true;
}

char* C3_Status_print(const C3_Status* status, const char* delimiter){
    int size =
        strlen(C3_OC_STATESTRING[status->occupationState]) +
        strlen(status->occupier) +
        strlen(C3_EM_STATESTRING[status->executionMode]) +
        strlen(C3_ES_STATESTRING[status->executionState]) +
        strlen(status->operationMode) +
        strlen(status->workingState) +
        strlen(status->error) +
        7 * strlen(delimiter) + 1;
    char* statusString = (char*) malloc(sizeof(char)*(size));
    sprintf(statusString, "%s%s%s%s%s%s%s%s%s%s%s%s%s",
        C3_OC_STATESTRING[status->occupationState], delimiter,
        status->occupier, delimiter,
        C3_EM_STATESTRING[status->executionMode], delimiter,
        C3_ES_STATESTRING[status->executionState], delimiter,
        status->operationMode, delimiter,
        status->workingState, delimiter,
        status->error);
    return statusString;
}

bool C3_Status_checkOpModeName(const char* opMode) {
    for (size_t i = 0; i < C3_OC_ORDERSIZE; i++)
    {
        if(strcmp(C3_OC_ORDERSTRING[i], opMode) == 0) {
            return false;
        }
    }
    for (size_t i = 0; i < C3_EM_ORDERSIZE; i++)
    {
        if(strcmp(C3_EM_ORDERSTRING[i], opMode) == 0) {
            return false;
        }
    }
    for (size_t i = 0; i < C3_ES_ORDERSIZE; i++)
    {
        if(strcmp(C3_ES_ORDERSTRING[i], opMode) == 0) {
            return false;
        }
    }
    return true;
}