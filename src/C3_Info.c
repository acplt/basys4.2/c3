#include "C3_Info.h"

bool C3_Info_init(C3_Info* info) {
    info->id = NULL;
    info->name = NULL;
    info->description = NULL;
    info->type = NULL;
    info->profile = C3_PROFILE_NULL;
    return true;
}

void C3_Info_clear(C3_Info* info) {
    if(info == NULL) return;
    free(info->id);
    free(info->name);
    free(info->description);
    free(info->type);
}

bool C3_Info_copy(const C3_Info* src, C3_Info* dest) {
    if(dest == NULL)
        return false;
    dest->id = malloc(sizeof(char) * (strlen(src->id)+1));
    dest->name = malloc(sizeof(char) * (strlen(src->name)+1));
    dest->description = malloc(sizeof(char) * (strlen(src->description)+1));
    dest->type = malloc(sizeof(char) * (strlen(src->type)+1));
    if(dest->id == NULL ||
       dest->name == NULL ||
       dest->description == NULL ||
       dest->type == NULL)
        return false;

    strcpy(dest->id, src->id);
    strcpy(dest->name, src->name);
    strcpy(dest->description, src->description);
    strcpy(dest->type, src->type);
    dest->profile = src->profile;
    return true;
}