#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <C3_Profiles.h>

int
C3_Profile_toFacetValue(C3_Profile profile, FACET_GROUP facet) {
    switch(facet) {
        case FACET_GROUP_SI:
            return profile.SI;
        case FACET_GROUP_OC:
            return profile.OC;
        case FACET_GROUP_EM:
            return profile.EM;
        case FACET_GROUP_ES:
            return profile.ES;
        case FACET_GROUP_OM:
            return profile.OM;
        default:
            return -1;
    }
}

const char *
C3_Profile_printFacet(const char **facets, int size, int value) {
    if(value) {
        // check for single values in possible values
        for(int i = 1; i < size; ++i) {
            if(value == (1 << (i - 1))) {
                return facets[i];
            }
        }
    } else
        // value is 0
        return facets[0];
    // value has multiple 1s
    return (value & 1) ? "NONE" : "ANY";
}

char *
C3_Profile_print(C3_Profile p) {
    // Check for standard profiles
    for(int i = 0; i < C3_PROFILES_COUNT; ++i) {
        if(C3_Profile_isEqual(p, C3_PROFILES[i])) {
            return strcpy(malloc(sizeof(char) * (strlen(C3_PROFILES_STRINGS[i]) + 1)),
                          C3_PROFILES_STRINGS[i]);
        }
    }

    // Write down custom profile values
    // TODO add parameter to print custom values also for standard profiles
    const char *si = C3_SI_TOSTRING(p.SI);
    const char *oc = C3_OC_TOSTRING(p.OC);
    const char *em = C3_EM_TOSTRING(p.EM);
    const char *es = C3_ES_TOSTRING(p.ES);
    const char *om = C3_OM_TOSTRING(p.OM);
    char *result = malloc(sizeof(char) * (20 + strlen(si) + strlen(oc) + strlen(em) +
                                          strlen(es) + strlen(om)));
    return sprintf(result, "SI:%s,OC:%s,EM:%s,ES:%s,OM:%s", si, oc, em, es, om) > 0
               ? result
               : NULL;
}

C3_Profile
C3_Profile_parse(char *string) {
    C3_Profile p = C3_PROFILE_UNKNOWN;
    // Check if defined profile is used
    for(int i = 0; i < C3_PROFILES_COUNT; ++i) {
        if(strcmp(string, C3_PROFILES_STRINGS[i]) == 0) {
            return C3_PROFILES[i];
        }
    }
    // Check if custom profile is given
    // Count elements and check if 6 elements are given
    int count = 1;
    for(int i = 0; string[i] != '\0'; i++) {
        if(string[i] == ',') {
            count++;
        }
    }
    if(count != 6)
        return p;

    // Try to cast values to facets
    // TODO check for int parser errors (use strol instead)
    // TODO check for invalid facet values
    char *delim = ",";
    p.SI = atoi(strtok(string, delim));
    p.OC = atoi(strtok(NULL, delim));
    p.EM = atoi(strtok(NULL, delim));
    p.ES = atoi(strtok(NULL, delim));
    p.OM = atoi(strtok(NULL, delim));
    p.optional = atoi(strtok(NULL, delim));
    return p;
}

bool
C3_Profile_isUnknown(const C3_Profile p) {
    return (p.SI == C3_SI_UNKNOWN) && (p.OC == C3_OC_UNKNOWN) &&
           (p.EM == C3_EM_UNKNOWN) && (p.ES == C3_ES_UNKNOWN) && (p.OM == C3_OM_UNKNOWN);
}

bool
C3_Profile_isEqual(const C3_Profile p1, const C3_Profile p2) {
    return (p1.SI == p2.SI) && (p1.OC == p2.OC) && (p1.EM == p2.EM) && (p1.ES == p2.ES) &&
           (p1.OM == p2.OM) && (p1.optional == p2.optional);
}
bool
C3_Profile_isEqualOrUnknown(const C3_Profile p1, const C3_Profile p2) {
    return (p1.SI == C3_SI_UNKNOWN || p2.SI == C3_SI_UNKNOWN || p1.SI == p2.SI) &&
           (p1.OC == C3_OC_UNKNOWN || p2.OC == C3_OC_UNKNOWN || p1.OC == p2.OC) &&
           (p1.EM == C3_EM_UNKNOWN || p2.EM == C3_EM_UNKNOWN || p1.EM == p2.EM) &&
           (p1.ES == C3_ES_UNKNOWN || p2.ES == C3_ES_UNKNOWN || p1.ES == p2.ES) &&
           (p1.OM == C3_OM_UNKNOWN || p2.OM == C3_OM_UNKNOWN || p1.OM == p2.OM);
}