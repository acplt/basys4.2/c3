#include "C3_InputOutput.h"

void C3_IOConfig_init(C3_IOConfig* config, C3_IO* io) {
    if(config->init)
        config->init(config->context, io);
    else {
        *config = C3_IOCONFIG_NULL;
        *io = NULL;
    }
}

void C3_IOConfig_clear(C3_IOConfig* config) {
    if(config == NULL) return;
    if(config->clear)
        config->clear(config->context);
    else {
        free(config->context);
    }
}