#include <C3_ExecutionMode.h>

C3_Result C3_EM_executeOrder(C3_EM_FACET facet, const C3_EM_Order order, const C3_EM_State state,
                               C3_EM_State* newState) {
    // Check EM facet
    if((facet == C3_EM_NONE) ||
            (order == C3_EM_ORDER_AUTO      && !(facet & C3_EM_AUTO))       ||
            (order == C3_EM_ORDER_SEMIAUTO  && !(facet & C3_EM_SEMIAUTO))   ||
            (order == C3_EM_ORDER_MANUAL    && !(facet & C3_EM_MANUAL))     ||
            (order == C3_EM_ORDER_SIMULATE  && !(facet & C3_EM_SIMULATE))   ){
        return C3_RESULT_BAD_PROFILE;
    }
    
    // EM state machine
    switch(state) {
        case C3_EM_STATE_AUTO:
            switch(order) {
                case C3_EM_ORDER_AUTO:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                case C3_EM_ORDER_SEMIAUTO:
                    *newState = C3_EM_STATE_SEMIAUTO;
                    break;
                case C3_EM_ORDER_MANUAL:
                    *newState = C3_EM_STATE_MANUAL;
                    break;
                case C3_EM_ORDER_SIMULATE:
                    *newState = C3_EM_STATE_SIMULATE;
                    break;
            }
            break;
        case C3_EM_STATE_SEMIAUTO:
            switch(order) {
                case C3_EM_ORDER_SEMIAUTO:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                case C3_EM_ORDER_AUTO:
                    *newState = C3_EM_STATE_AUTO;
                    break;
                case C3_EM_ORDER_MANUAL:
                    *newState = C3_EM_STATE_MANUAL;
                    break;
                case C3_EM_ORDER_SIMULATE:
                    *newState = C3_EM_STATE_SIMULATE;
                    break;        
            }
            break;
        case C3_EM_STATE_MANUAL:
            switch(order) {
                case C3_EM_ORDER_MANUAL:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                case C3_EM_ORDER_AUTO:
                    *newState = C3_EM_STATE_AUTO;
                    break;
                case C3_EM_ORDER_SEMIAUTO:
                    *newState = C3_EM_STATE_SEMIAUTO;
                    break;
                case C3_EM_ORDER_SIMULATE:
                    *newState = C3_EM_STATE_SIMULATE;
                    break;                   
            }
            break;
        case C3_EM_STATE_SIMULATE:
            switch(order) {
                case C3_EM_ORDER_SIMULATE:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                case C3_EM_ORDER_AUTO:
                    *newState = C3_EM_STATE_AUTO;
                    break;
                case C3_EM_ORDER_MANUAL:
                    *newState = C3_EM_STATE_MANUAL;
                    break;
                case C3_EM_ORDER_SEMIAUTO:
                    *newState = C3_EM_STATE_SEMIAUTO;
                    break;                 
            }
            break;
        default:
            return C3_RESULT_BAD_STATE;
    }
    return C3_RESULT_GOOD;
}