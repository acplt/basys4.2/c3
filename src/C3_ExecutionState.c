
#include <C3_ExecutionState.h>

// ES state machine (full packml)
static C3_Result packMLStateMachine(const C3_ES_Order order, const C3_ES_State state,
                               C3_ES_State* newState) {
    switch(state) {
        case C3_ES_STATE_ABORTING:
            switch(order) {
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_ABORTED;
                    break;
                case C3_ES_ORDER_ABORT:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_ABORTED:
            switch(order) {
                case C3_ES_ORDER_CLEAR:
                    *newState = C3_ES_STATE_CLEARING;
                    break;
                case C3_ES_ORDER_ABORT:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_CLEARING:
        case C3_ES_STATE_STOPPING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;

                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_STOPPED;
                    break;
                case C3_ES_ORDER_STOP:
                case C3_ES_ORDER_CLEAR:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_STOPPED:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_RESET:
                    *newState = C3_ES_STATE_RESETTING;
                    break;
                case C3_ES_ORDER_STOP:
                case C3_ES_ORDER_CLEAR:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_RESETTING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_IDLE;
                    break;
                case C3_ES_ORDER_RESET:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_IDLE:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_START:
                    *newState = C3_ES_STATE_STARTING;
                    break;
                case C3_ES_ORDER_RESET:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_STARTING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_HOLD:
                    *newState = C3_ES_STATE_HOLDING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_EXECUTE;
                    break;
                case C3_ES_ORDER_START:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_EXECUTE:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_HOLD:
                    *newState = C3_ES_STATE_HOLDING;
                    break;
                case C3_ES_ORDER_SUSPEND:
                    *newState = C3_ES_STATE_SUSPENDING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_COMPLETING;
                    break;
                case C3_ES_ORDER_START:
                case C3_ES_ORDER_UNHOLD:
                case C3_ES_ORDER_UNSUSPEND:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_COMPLETING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_COMPLETE;
                    break;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_COMPLETE:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_RESET:
                    *newState = C3_ES_STATE_RESETTING;
                    break;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_SUSPENDING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_HOLD:
                    *newState = C3_ES_STATE_HOLDING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_SUSPENDED;
                    break;
                case C3_ES_ORDER_SUSPEND:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_SUSPENDED:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_HOLD:
                    *newState = C3_ES_STATE_HOLDING;
                    break;
                case C3_ES_ORDER_UNSUSPEND:
                    *newState = C3_ES_STATE_UNSUSPENDING;
                    break;
                case C3_ES_ORDER_SUSPEND:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_UNSUSPENDING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_HOLD:
                    *newState = C3_ES_STATE_HOLDING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_EXECUTE;
                    break;
                case C3_ES_ORDER_UNSUSPEND:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_HOLDING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_HELD;
                    break;
                case C3_ES_ORDER_HOLD:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_HELD:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_UNHOLD:
                    *newState = C3_ES_STATE_UNHOLDING;
                    break;
                case C3_ES_ORDER_HOLD:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        case C3_ES_STATE_UNHOLDING:
            switch(order) {
                case C3_ES_ORDER_ABORT:
                    *newState = C3_ES_STATE_ABORTING;
                    break;
                case C3_ES_ORDER_STOP:
                    *newState = C3_ES_STATE_STOPPING;
                    break;
                case C3_ES_ORDER_SC:
                    *newState = C3_ES_STATE_EXECUTE;
                    break;
                case C3_ES_ORDER_UNHOLD:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                default:
                    return C3_RESULT_BAD_ORDER;
            }
            break;
        default:
            return C3_RESULT_BAD_STATE;
    }
    return C3_RESULT_GOOD;
}

C3_Result C3_ES_executeOrder(C3_ES_FACET facet, const C3_ES_Order order, const C3_ES_State state,
                               C3_ES_State* newState) {
    // Check ES facet
    switch(facet) {
        case C3_ES_NONE:
            return C3_RESULT_BAD_PROFILE;
        case C3_ES_UNKNOWN:
        case C3_ES_BASYS:
            if(     (order == C3_ES_ORDER_HOLD)     ||
                    (order == C3_ES_ORDER_UNHOLD)   ||
                    (order == C3_ES_ORDER_SUSPEND)  ||
                    (order == C3_ES_ORDER_UNSUSPEND) )
                return C3_RESULT_BAD_PROFILE;
        case C3_ES_PACKML:
            return packMLStateMachine(order, state, newState);
        default:
            return C3_RESULT_BAD_NOTIMPLEMENTED;
    }
}