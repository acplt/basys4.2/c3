#include <C3_Occupation.h>

static C3_Result
copyOccupier(const char* src, char** dest, char** last){
    if(dest == NULL)
        return C3_RESULT_BAD;
    if(strcmp(src, *dest) == 0){
        return C3_RESULT_GOOD;
    }

    char* newDest = (char*) malloc(sizeof(char) * (strlen(src)+1));
    if(newDest == NULL)
        return C3_RESULT_BAD_MEMORY;
    strcpy(newDest, src);

    free(*last);
    *last = *dest;
    *dest = newDest;
    return C3_RESULT_GOOD;
}

C3_Result C3_OC_executeOrder(C3_OC_FACET facet, const char* sender, const C3_OC_Order order, const C3_OC_State state,
                       C3_OC_State* newState, char** occupier, char** lastOccupier) {
    // Check OC facet
    if((facet == C3_OC_NONE) ||
            (order == C3_OC_ORDER_FREE      && !(facet & (C3_OC_OCCUPIED|C3_OC_PRIO)))  ||
            (order == C3_OC_ORDER_OCCUPY    && !(facet & C3_OC_OCCUPIED))               ||
            (order == C3_OC_ORDER_PRIO      && !(facet & C3_OC_PRIO))                   ){
        return C3_RESULT_BAD_PROFILE;
    }
    
    // OC state machine
    if(sender == NULL)
        sender = C3_OC_OCCUPIER_ANONYM;
    if(strcmp(sender,C3_OC_OCCUPIER_FREE) == 0)
        return C3_RESULT_BAD_ACCESS;
    switch(state) {
        case C3_OC_STATE_FREE:
            switch(order) {
                case C3_OC_ORDER_FREE:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                case C3_OC_ORDER_OCCUPY:
                    *newState = C3_OC_STATE_OCCUPIED;
                    return copyOccupier(sender, occupier, lastOccupier);
                case C3_OC_ORDER_PRIO:
                    *newState = C3_OC_STATE_PRIO;
                    return copyOccupier(sender, occupier, lastOccupier);
            }
            break;
        case C3_OC_STATE_OCCUPIED:
            switch(order) {
                case C3_OC_ORDER_FREE:
                    if(strcmp(sender, *occupier) != 0)
                        return C3_RESULT_BAD_ACCESS;
                    *newState = C3_OC_STATE_FREE;
                    return copyOccupier(C3_OC_OCCUPIER_FREE, occupier, lastOccupier);
                case C3_OC_ORDER_OCCUPY:
                    if(strcmp(sender, *occupier) != 0)
                        return C3_RESULT_BAD_ACCESS;
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
                case C3_OC_ORDER_PRIO:
                    *newState = C3_OC_STATE_PRIO;
                    return copyOccupier(sender, occupier, lastOccupier);
            }
            break;
        case C3_OC_STATE_PRIO:
            if(strcmp(sender, *occupier) !=0 )
                return C3_RESULT_BAD_ACCESS;
            switch(order) {
                case C3_OC_ORDER_FREE:
                    *newState = C3_OC_STATE_FREE;
                    return copyOccupier(C3_OC_OCCUPIER_FREE, occupier, lastOccupier);
                case C3_OC_ORDER_OCCUPY:
                    *newState = C3_OC_STATE_OCCUPIED;
                    break;
                case C3_OC_ORDER_PRIO:
                    return C3_RESULT_GOOD_TARGETSTATEACTIVE;
            }
            break;
        case C3_OC_STATE_LOCAL:
            return C3_RESULT_BAD_ACCESS;
        default:
            return C3_RESULT_BAD_STATE;
    }
    return C3_RESULT_GOOD;
}