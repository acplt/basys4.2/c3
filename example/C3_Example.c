#include <stdio.h>
#include <stdlib.h>

#include "C3_ControlComponent.h"

/*
 * Input Output configuration
 */
struct MyIO {
    int input1;
    int output1;
};

static void
MyIO_init(void *context, C3_IO *io) {
    *io = calloc(1,sizeof(struct MyIO));
    printf(" - Initialized IOs.\n");
}

static void
MyIO_read(void *context, C3_IO io) {
    struct MyIO *myIo = (struct MyIO *)io;
    myIo->input1 = rand() % 101;  // random value from 0 to 100
    printf(" - Reading inputs: input1 = %i\n", myIo->input1);
}

static void
MyIO_write(void *context, C3_IO io) {
    struct MyIO *myIo = (struct MyIO *)io;
    printf(" - Writing outputs: output1 = %i\n", myIo->output1);
}

static void
MyIO_readSim(void *context, C3_IO io) {
    struct MyIO *myIo = (struct MyIO *)io;
    myIo->input1 = 42;
    printf(" - Simulate reading inputs: input1 = %i\n", myIo->input1);
}

static void
MyIO_writeSim(void *context, C3_IO io) {
    struct MyIO *myIo = (struct MyIO *)io;
    printf(" - Simulate writing outputs: output1 = %i\n", myIo->output1);
}

static void
MyIO_interlock(void *context, C3_IO io) {
    struct MyIO *myIo = (struct MyIO *)io;
    if(myIo->output1 > 5000)
            myIo->output1 = 5000;
}

/*
 * Definition of a dummy operation mode
 */

bool
MyOpMode_init(C3_CC *cc, C3_OP_OpMode *opMode) {
    printf(" - Initialising %s for %s.\n", opMode->name, C3_CC_getId(cc));
    *((int *)opMode->context) = 0;
    return true;
}

void
MyOpMode_execute(C3_CC *cc, C3_OP_OpMode *opMode, C3_IO io, C3_ES_Order *order,
                 const char **workingState, const char **errorState) {
    int *counter = ((int *)opMode->context);
    C3_Status status = C3_CC_getStatus(cc);
    printf("%6i Executing %s for %s.", *counter, opMode->name, C3_CC_getId(cc));
    if(status.executionState == C3_ES_STATE_EXECUTE) {
        if(*counter > 3) {
            *order = C3_ES_ORDER_SC;
            printf(" Execution complete. Trigger internal state change (SC).\n");
        } else {
            struct MyIO *myIo = (struct MyIO *)io;
            myIo->output1 = myIo->input1 * myIo->input1 + 42;
            printf(" Do something f(%i)=%i.\n", myIo->input1, myIo->output1);
        }
    } else if(C3_ES_ISACTIVESTATE[status.executionState]) {
        *order = C3_ES_ORDER_SC;
        printf(" Trigger internal state change (SC).\n");
    } else {
        printf(" Nothing to do.\n");
    }
    (*counter)++;
}

void
MyOpMode_clear(C3_CC *cc, C3_OP_OpMode *opMode) {
    printf(" - Clearing %s from %s after %i executions.\n", opMode->name, C3_CC_getId(cc),
           *(int *)opMode->context);
    *((int *)opMode->context) = -1;
}

bool
MyOpMode_select(C3_CC *cc, C3_OP_OpMode *opMode) {
    printf(" - Selecting %s for %s.\n", opMode->name, C3_CC_getId(cc));
    return true;
}

bool
MyOpMode_deselect(C3_CC *cc, C3_OP_OpMode *opMode) {
    printf(" - Deselecting %s for %s.\n", opMode->name, C3_CC_getId(cc));
    return true;
}

/*
 * Utility functions for the example
 */

static void
sendCMD(C3_CC *cc, char *cmd) {
    printf("%s recieved command: %s\n", C3_CC_getId(cc), cmd);
    C3_CC_CmdString(cc, cmd);
    char *status = C3_CC_printStatus(cc);
    printf(" - Status: %s\n", status);
    free(status);
}

/*
 * Main example function
 */

int
main() {
    printf("--- Create a New Control Component ---\n");
    C3_CC *cc = C3_CC_new();

    /* Set and get info */
    C3_Info info;
    info.id = "MyCC";
    info.name = "My Control Component";
    info.description = "An example control component of the C3 project.";
    info.profile = C3_PROFILE_BASYS;
    info.type = "MyCCType";
    C3_CC_setInfo(cc, &info);

    const C3_Info info2 = C3_CC_getInfo(cc);
    char *profileStr = C3_Profile_print(info2.profile);
    printf("Info:\n\tId: %s\n\tName: %s\n\tDescription: %s\n\tProfile: %s\n\tType: %s\n",
           info2.id, info2.name, info2.description, profileStr, info2.type);

    /* Get and print status */
    const C3_Status status = C3_CC_getStatus(cc);
    char *statusString = C3_Status_print(&status, ",\n\t");
    printf("Status:\n\t%s\n", statusString);
    free(statusString);

    /* Add io configuration*/
    printf("Set IO configuration for MyCC\n");
    C3_IOConfig ioConfig = C3_IOCONFIG_NULL;
    ioConfig.init = MyIO_init;
    ioConfig.read = MyIO_read;
    ioConfig.write = MyIO_write;
    ioConfig.interlock = MyIO_interlock;
    ioConfig.readSim = MyIO_readSim;
    ioConfig.writeSim = MyIO_writeSim;
    C3_CC_setIOConfig(cc, ioConfig);

    /* Add and list operation modes */
    printf("\n--- Add Operation Modes ---\n");
    printf("Add MyOpMode to MyCC\n");
    int myOpModeCounter = -1;
    C3_OP_OpMode myOpMode;
    myOpMode.context = &myOpModeCounter;
    myOpMode.name = "MyOpMode";
    myOpMode.init = MyOpMode_init;
    myOpMode.execute = MyOpMode_execute;
    myOpMode.clear = MyOpMode_clear;
    myOpMode.select = MyOpMode_select;
    myOpMode.deselect = MyOpMode_deselect;
    C3_CC_addOperationMode(cc, &myOpMode);

    printf("Add MyOtherOpMode to MyCC\n");
    int myOpModeCounter2 = -1;
    myOpMode.name = "MyOtherOpMode";
    myOpMode.context = &myOpModeCounter2;
    C3_CC_addOperationMode(cc, &myOpMode);

    char **operationModeNames = NULL;
    size_t operationModeCount = C3_CC_getOperationModeNames(cc, &operationModeNames);
    printf("Added %zu OperationModes:\n", operationModeCount);
    for(size_t i = 0; i < operationModeCount; i++) {
        printf("\t%s\n", operationModeNames[i]);
        free(operationModeNames[i]);
        operationModeNames[i] = NULL;
    }
    free(operationModeNames);
    operationModeNames = NULL;

    /* Execute control component */
    printf("\n--- Execute Control Component ---\n");
    // Run 3 cycles
    for(size_t i = 0; i < 3; i++) {
        // typically this is done in a cyclic task / thread
        C3_CC_runIterate(cc);
    }

    // Occupy and reset component
    sendCMD(cc, "Orchestrator;OCCUPY;");

    // Select operation mode via Cmd instead of CmdString (to show its usage)
    C3_Command cmd;
    cmd.type = C3_CMDTYPE_OP;
    cmd.order.str = "MyOpMode";
    cmd.sender = "Orchestrator";
    cmd.paramCount = 2;
    cmd.paramKeys = (char *[]){"param1", "param2"};
    cmd.paramVals = (char *[]){"value1", "value2"};
    char *cmdString = C3_Command_print(&cmd);
    printf("%s recieved command: %s\n", C3_CC_getId(cc), cmdString);
    free(cmdString);
    C3_CC_Cmd(cc, &cmd);
    statusString = C3_CC_printStatus(cc);
    printf(" - Status: %s\n", statusString);
    free(statusString);

    // Start and run some cycles
    sendCMD(cc, "Orchestrator;RESET;");
    C3_CC_runIterate(cc);
    sendCMD(cc, "Orchestrator;START;");
    for(size_t i = 0; i < 5; i++) {
        // typically this is done in a cyclic task / thread
        C3_CC_runIterate(cc);
    }
    sendCMD(cc, "Orchestrator;STOP;");
    C3_CC_runIterate(cc);

    // Let the operator take over control and change to SIMULATE
    sendCMD(cc, "Operator;PRIO;");
    sendCMD(cc, "Operator;SIMULATE;");

    // Select other operation mode
    sendCMD(cc, "Operator;MyOtherOpMode;p1=v1,p2=v2");
    sendCMD(cc, "Operator;RESET;");
    C3_CC_runIterate(cc);
    sendCMD(cc, "Operator;START;");
    for(size_t i = 0; i < 5; i++) {
        C3_CC_runIterate(cc);
    }

    /* Delete Operation Mode */
    printf("\n--- Delete Operation Mode ---\n");
    printf("Delete MyOtherOpMode from MyCC\n");
    C3_CC_deleteOperationMode(cc, "MyOtherOpMode");
    statusString = C3_CC_printStatus(cc);
    printf(" - Status: %s\n", statusString);
    free(statusString);

    // Select another operation mode
    sendCMD(cc, "Operator;MyOpMode;");
    // Free component
    sendCMD(cc, "Operator;FREE;");

    /* Free CC resources */
    printf("\n--- Free Resources ---\n");
    printf("Clear MyCC\n");
    C3_CC_clear(cc);
    free(cc);
}