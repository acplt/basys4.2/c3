# C3 - A Control Component C Implementation

This project provides a simple C library to create [Control Components](http://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponent) in specific variants called [profiles](http://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles).

It can be extended with an OPC UA Server to provide a network interface, as shown in the [CCProfilesUA](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA) Project.

## Further Reading

### Control Component Concepts

* BaSyx Wiki: https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponent and https://wiki.eclipse.org/BaSyx_/_Documentation_/_ControlComponent
* Metamodel from [BaSys 4.0 project](https://basys40.de) (german): http://publications.rwth-aachen.de/record/728724/files/728724.pdf
* Changeability and interoperability in I4.0 (german): https://link.springer.com/referenceworkentry/10.1007%2F978-3-662-45537-1_144-1
* Command interface for IEC 61131 (german): https://publications.rwth-aachen.de/record/766365
* Background on component reuse (german): https://download.fortiss.org/public/mbees/mbees2018_proceedings.pdf

### Variants

* Concept  and metamodel for variant description: https://wiki.eclipse.org/BaSyx_/_Documentation_/_ControlComponentProfiles 
* Facetts and profiles:  https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles
* Comparison to NAMUR MTP and OMAC PackML: https://elibrary.vdi-verlag.de/10.51202/9783181023754-899/cross-industry-state-of-the-art-analysis-of-modular-automation?page=1

### Background

* Comparison of execution state and mode models: https://doi.org/10.17560/atp.v61i9.2398
* Usage in combination with virtualization: https://publications.rwth-aachen.de/record/728663
* Usage for machine learningn integration: https://link.springer.com/chapter/10.1007%2F978-3-030-69367-1_4
* Usage for assesment of machine learning tasks complexity: https://doi.org/10.1515/auto-2021-0118
* Orchestration of control component skills: https://doi.org/10.1109/ETFA45728.2021.9613728

### Student Thesis

Mostly german thesis available at the [Chair of Information- and Automationsystems for Process- and Material Technology](https://www.plt.rwth-aachen.de)

* 2018, Bachelorarbeit, Markus Gelfgren: Realisierung einer Assetverwaltung für die operative Prozessführung eines automatisierten Fördersystems
* 2019, Bachelorarbeit, Jannis Schulz: Planung und Implementierung von BaSys 4.0 Komponenten zur Realisierung eines Aufdienvorgangs
* 2019, Bachelorarbeit, Tjerk-Ove Wienkoop: Anbindung einer BPMN-Engine an das BaSys 4.0 Komponentensystem am Beispiel einer Simulation eines Kaltwalzwerks unter Berücksichtigung der IEC 61512
* 2019, Minithesis, Meng Gao: Realization of a BaSys 4.0 automation control for an inspection station in an aluminium cold rolling mill
* 2019, Minithesis, Ocson Reginald Cocen: Realization of a BaSys 4.0 Automation Control for a Sleeve Handling Process in an Aluminum Cold Rolling Mill
* 2019, Minithesis, Yan Kaichun: Engineering of Roller Table on ABB Station
* 2019, Minithesis, Shieren Sumarli: Realization of a BaSys 4.0 Automation Control for a Rolling Process in an Aluminium Cold Rolling Mill
* 2019, Hauptseminararbeit, Leonard Woeste: Wandelbarkeit durch modulare Prozessführungs-Komponenten
* 2021, Masterarbeit, Tobias Rink: Integrationsvarianten von Reinforcement Learning Agenten in BaSys-Führungskomponenten

## Software Projects

* OPC UA interface implementations for different profiles and compliance tests: https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA
* BaSyx SDKs, e.g.: https://wiki.eclipse.org/BaSyx_/_Introductory_Examples or https://github.com/eclipse-basyx
* DFKI: https://github.com/BaSys-PC1/control-component
