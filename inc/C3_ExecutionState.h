#ifndef C3_EXECUTIONSTATE_H
#define C3_EXECUTIONSTATE_H

#include <stdbool.h>

#include "C3_Result.h"
#include "C3_Profiles.h"

/*
 * Control component state machine for execution states
 */
typedef enum {
    C3_ES_ORDER_START,
    C3_ES_ORDER_STOP,
    C3_ES_ORDER_RESET,
    C3_ES_ORDER_ABORT,
    C3_ES_ORDER_CLEAR,
    C3_ES_ORDER_HOLD,
    C3_ES_ORDER_UNHOLD,
    C3_ES_ORDER_SUSPEND,
    C3_ES_ORDER_UNSUSPEND,
    C3_ES_ORDER_SC  // Special "internal" order from the machine side: StateComplete
} C3_ES_Order;
#define C3_ES_ORDERSIZE 10
static const char *C3_ES_ORDERSTRING[] = {"START", "STOP",   "RESET",   "ABORT",     "CLEAR",
                                       "HOLD",  "UNHOLD", "SUSPEND", "UNSUSPEND", "SC"};

/* Numbers according to PACKML OPC UA StateValue (StateNumber) -1 as they start from 1:
 * https://reference.opcfoundation.org/v104/PackML/v100/docs/B.1.1/
 * Cleared and Run are macro states defined by PACKML (value 18, 19)
 */
typedef enum {
    C3_ES_STATE_CLEARING,
    C3_ES_STATE_STOPPED,
    C3_ES_STATE_STARTING,
    C3_ES_STATE_IDLE,
    C3_ES_STATE_SUSPENDED,
    C3_ES_STATE_EXECUTE,
    C3_ES_STATE_STOPPING,
    C3_ES_STATE_ABORTING,
    C3_ES_STATE_ABORTED,
    C3_ES_STATE_HOLDING,
    C3_ES_STATE_HELD,
    C3_ES_STATE_UNHOLDING,
    C3_ES_STATE_SUSPENDING,
    C3_ES_STATE_UNSUSPENDING,
    C3_ES_STATE_RESETTING,
    C3_ES_STATE_COMPLETING,
    C3_ES_STATE_COMPLETE
} C3_ES_State;
#define C3_ES_STATESIZE 17
static const char *C3_ES_STATESTRING[] = {
    "CLEARING",   "STOPPED",      "STARTING",  "IDLE",       "SUSPENDED", "EXECUTE",
    "STOPPING",   "ABORTING",     "ABORTED",   "HOLDING",    "HELD",      "UNHOLDING",
    "SUSPENDING", "UNSUSPENDING", "RESETTING", "COMPLETING", "COMPLETE"};

C3_Result C3_ES_executeOrder(C3_ES_FACET facet, const C3_ES_Order order, const C3_ES_State state,
                             C3_ES_State* newState);

static const bool C3_ES_ISACTIVESTATE[] = {
    true,  // C3_ES_STATE_CLEARING,
    false, // C3_ES_STATE_STOPPED,
    true,  // C3_ES_STATE_STARTING,
    false, // C3_ES_STATE_IDLE,
    false, // C3_ES_STATE_SUSPENDED,
    true,  // C3_ES_STATE_EXECUTE,
    true,  // C3_ES_STATE_STOPPING,
    true,  // C3_ES_STATE_ABORTING,
    false, // C3_ES_STATE_ABORTED,
    true,  // C3_ES_STATE_HOLDING,
    false, // C3_ES_STATE_HELD,
    true,  // C3_ES_STATE_UNHOLDING,
    true,  // C3_ES_STATE_SUSPENDING,
    true,  // C3_ES_STATE_UNSUSPENDING,
    true,  // C3_ES_STATE_RESETTING,
    true,  // C3_ES_STATE_COMPLETING,
    false, // C3_ES_STATE_COMPLETE
};

#endif /* C3_EXECUTIONSTATE_H */