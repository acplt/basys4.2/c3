#ifndef C3_COMMAND_H
#define C3_COMMAND_H

#include "C3_Status.h"

typedef enum C3_CommandType {
    C3_CMDTYPE_UNKNOWN,
    C3_CMDTYPE_OC,
    C3_CMDTYPE_EM,
    C3_CMDTYPE_ES,
    C3_CMDTYPE_OP
} C3_CommandType;

typedef struct {
    char* sender;
    C3_CommandType type;
    union {
        int num;
        char* str;
    } order;
    int paramCount;
    char** paramKeys;
    char** paramVals;
} C3_Command;

C3_Command* C3_Command_new(void);

void C3_Command_clear(C3_Command* cmd);

char* C3_Command_print(C3_Command* cmd);

C3_Command* C3_Command_parse(const char* cmd);

void C3_Command_parseOrder(C3_Command* cmd);

#endif /* C3_COMMAND_H */