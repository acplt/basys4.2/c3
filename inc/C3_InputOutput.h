#ifndef C3_INPUTOUTPUT_H
#define C3_INPUTOUTPUT_H

#include <stdlib.h>

typedef void* C3_IO;

/* Defines callbacks, to read or write inputs and outputs, e.g. via a fieldbus, shared memory, network client, ...
 * The control component calls read, execute (of the active operation mode), interlock and write in a cyclic manner via C3_CC_RunIterate();
 * If the execution mode is set to SIMULATE readSim and writeSim are called instead.
 * The context pointer is used to store handles, adresses, client configuration, etc.
 * To free its resources the clear callback is used.
 */ 
typedef struct C3_IOConfig {
    void* context;
    void (*init)(void* context, C3_IO* io);
    void (*clear)(void* context);
    void (*read)(void* context, C3_IO io);
    void (*write)(void* context, C3_IO io);
    void (*readSim)(void* context, C3_IO io);
    void (*writeSim)(void* context, C3_IO io);
    void (*interlock)(void* context, C3_IO io);
} C3_IOConfig;

static const C3_IOConfig C3_IOCONFIG_NULL = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

void C3_IOConfig_init(C3_IOConfig* config, C3_IO* io);

void C3_IOConfig_clear(C3_IOConfig* config);

#endif /* C3_INPUTOUTPUT_H */