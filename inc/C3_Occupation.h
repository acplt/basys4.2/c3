#ifndef C3_OCCUPATION_H
#define C3_OCCUPATION_H

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "C3_Result.h"
#include "C3_Profiles.h"

/*
 * Control component state machine for occupation
 */
typedef enum {
    C3_OC_ORDER_FREE,
    C3_OC_ORDER_OCCUPY,
    C3_OC_ORDER_PRIO
} C3_OC_Order;
#define C3_OC_ORDERSIZE 3
static const char *C3_OC_ORDERSTRING[] = {"FREE", "OCCUPY", "PRIO"};

typedef enum {
    C3_OC_STATE_FREE,
    C3_OC_STATE_OCCUPIED,
    C3_OC_STATE_PRIO,
    C3_OC_STATE_LOCAL
} C3_OC_State;
#define C3_OC_STATESIZE 4
static const char *C3_OC_STATESTRING[] = {"FREE", "OCCUPIED", "PRIO", "LOCAL"};

static const char* C3_OC_OCCUPIER_FREE = "NONE";
static const char* C3_OC_OCCUPIER_ANONYM = "Anonymous";

C3_Result C3_OC_executeOrder(C3_OC_FACET facet, const char* sender, const C3_OC_Order order, const C3_OC_State state,
                             C3_OC_State* newState, char** occupier, char** lastOccupier);

#endif /* C3_OCCUPATION_H */