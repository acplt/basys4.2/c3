#ifndef C3_STATUS_H
#define C3_STATUS_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "C3_Occupation.h"
#include "C3_ExecutionMode.h"
#include "C3_ExecutionState.h"
#include "C3_OperationMode.h"

static const char* C3_WORKSTATE_NONE = "NONE";
static const char* C3_ERRORSTATE_NONE = "OK";

/* Status defines the current state of the control component state machines etc. */
typedef struct C3_Status {
    char* lastCMD;
    C3_OC_State occupationState;
    char* occupier;
    char* lastOccupier;
    C3_EM_State executionMode;
    C3_ES_State executionState;
    const char* operationMode;
    char* workingState;
    char* error;
    char* lastError;
} C3_Status;

bool C3_Status_init(C3_Status* status);

void C3_Status_clear(C3_Status* status);

bool C3_Status_copy(const C3_Status* src, C3_Status* dest);

char* C3_Status_print(const C3_Status* status, const char* delimiter);

bool C3_Status_checkOpModeName(const char* opMode);

#endif /* C3_STATUS_H */