/*
 ============================================================================
 Name        : C3_Profiles.h
 Author      : Julian Grothoff
 Version     : 0.2
 Copyright   : PLT
 Description : Common definitions for facets, facet groups and profiles
               Description can be found in:
https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles
 ============================================================================
 */
#ifndef C3_PROFILES_H
#define C3_PROFILES_H

#include <stdbool.h>

/*
 * SI: Service interface facet names and numbers
 * defined in BaSys 4.2 Deliverable D-3.2
 */
typedef enum C3_SI_FACET {
    C3_SI_UNKNOWN = 0,
    C3_SI_NONE = 1,
    C3_SI_CMD = 2,
    C3_SI_OPERATIONS = 4,
    C3_SI_SERVICES = 8,
    C3_SI_MTP = 16,
    C3_SI_PACKML = 32,
    C3_SI_FULL =
        (C3_SI_CMD | C3_SI_OPERATIONS | C3_SI_SERVICES | C3_SI_MTP | C3_SI_PACKML)
} C3_SI_FACET;
#define C3_SI_FACET_COUNT 7
#define C3_SI_FACET_MAXVALUE (C3_SI_PACKML * 2 - 1)
static const char *C3_SI_FACET_STRINGS[] = {"UNKNOWN",  "NONE", "CMD",   "OPERATIONS",
                                            "SERVICES", "MTP",  "PACKML"};
#define C3_SI_TOSTRING(value)                                                            \
    C3_Profile_printFacet(C3_SI_FACET_STRINGS, C3_SI_FACET_COUNT, value)

/*
 * OC: Occupation facet names and numbers
 * defined in BaSys 4.2 Deliverable D-4.6
 */
typedef enum C3_OC_FACET {
    C3_OC_UNKNOWN = 0,   // Undefined occupation profiles
    C3_OC_NONE = 1,      // No occupation available / necessary
    C3_OC_OCCUPIED = 2,  // Only one occupier (OCCST = FREE, OCCUPIED)
    C3_OC_PRIO = 4,      // Priority occupation possible (OCCST = FREE, PRIO)
    C3_OC_LOCAL = 8,     // Local occupation possible (OCCST = FREE, LOCAL)
    C3_OC_FULL = (C3_OC_OCCUPIED | C3_OC_PRIO | C3_OC_LOCAL)
} C3_OC_FACET;
#define C3_OC_FACET_COUNT 5
static const char *C3_OC_FACET_STRINGS[] = {"UNKNOWN", "NONE", "OCCUPIED", "PRIO",
                                            "LOCAL"};
#define C3_OC_TOSTRING(value)                                                            \
    C3_Profile_printFacet(C3_OC_FACET_STRINGS, C3_OC_FACET_COUNT, value)

/*
 * EM: Execution mode facet names and numbers
 * defined in BaSys 4.2 Deliverable D-4.6
 */
typedef enum C3_EM_FACET {
    C3_EM_UNKNOWN = 0,    // Undefined execution mode profiles
    C3_EM_NONE = 1,       // No execution modes to select (=Always in AUTO)
    C3_EM_AUTO = 2,       // EXMODE = AUTO
    C3_EM_MANUAL = 4,     // EXMODE = MANUAL
    C3_EM_SEMIAUTO = 8,   // EXMODE = SEMIAUTO
    C3_EM_SIMULATE = 16,  // EXMODE = SIMUALTE
    C3_EM_FULL = (C3_EM_AUTO | C3_EM_MANUAL | C3_EM_SEMIAUTO | C3_EM_SIMULATE)
} C3_EM_FACET;
#define C3_EM_FACET_COUNT 6
static const char *C3_EM_FACET_STRINGS[] = {"UNKNOWN", "NONE",     "AUTO",
                                            "MANUAL",  "SEMIAUTO", "SIMULATE"};
#define C3_EM_TOSTRING(value)                                                            \
    C3_Profile_printFacet(C3_EM_FACET_STRINGS, C3_EM_FACET_COUNT, value)
/*
 * ES: Execution state facet names and numbers
 * defined in BaSys 4.2 Deliverable D-4.6
 * Only BASYS and PACKML profile is supported right now
 */
typedef enum C3_ES_FACET {
    C3_ES_UNKNOWN = 0,  // Undefined execution state profile
    C3_ES_NONE = 1,     // No execution states to select (=Always in EXECUTE)
    C3_ES_BASYS = 2,    // Subset of PackML (Start, Stop, Reset, Abort)
    C3_ES_PACKML = 3,   // Full PackML state machine
    C3_ES_ISA88 = 4,    // ISA 88 procedural elements state machine
    C3_ES_MTP = 5,      // MTP state machine
    C3_ES_OPCUA = 6,    // OPC UA program state machine (Part 10)
    C3_ES_NE160 = 7     // NE160 procedural state machine
} C3_ES_FACET;
#define C3_ES_FACET_COUNT 8
static const char *C3_ES_FACET_STRINGS[] = {"UNKNOWN", "NONE", "BASYS", "PACKML",
                                            "ISA88",   "MTP",  "OPCUA", "NE160"};
// TODO check for C3_ES_FACET_COUNT:
#define C3_ES_TOSTRING(value) C3_ES_FACET_STRINGS[value]
/*
 * OP: Operation mode facet names and numbers
 * defined in BaSys 4.2 Deliverable D-4.6
 */
typedef enum C3_OM_FACET {
    C3_OM_UNKNOWN = 0,  // Undefined operation mode profile
    C3_OM_NONE = 1,     // No operation modes to select (= automatically select first operation mode)
    C3_OM_MULTI = 2,    // Multiple operation modes
    C3_OM_PARALLEL =
        3,           // Parallel executable operation modes, each has its own execution
                     // state, but the overall execution state has to be aggregated
    C3_OM_CONTI = 4  // Conti process operation mode. BSTATE, STARTUP, SHUTDOWN

} C3_OM_FACET;
#define C3_OM_FACET_COUNT 5
static const char *C3_OM_FACET_STRINGS[] = {"UNKNOWN", "NONE", "MULTI", "PARALLEL",
                                            "CONTI"};
// TODO check for C3_OM_FACET_COUNT:
#define C3_OM_TOSTRING(value) C3_OM_FACET_STRINGS[value]

/*
 * Structure to combine multiple facets in one full featured profile value
 * defined in BaSys 4.2 Deliverable D-4.6 and D-4.7b (Integrationvariants)
 */
typedef struct C3_Profile {
    C3_SI_FACET SI;
    C3_OC_FACET OC;
    C3_EM_FACET EM;
    C3_ES_FACET ES;
    C3_OM_FACET OM;
    bool optional;
} C3_Profile;

typedef enum FACET_GROUP {
    FACET_GROUP_SI,
    FACET_GROUP_OC,
    FACET_GROUP_EM,
    FACET_GROUP_ES,
    FACET_GROUP_OM
} FACET_GROUP;

/* New C3_PROFILES */
#define C3_PROFILE_UNKNOWN                                                               \
    (C3_Profile) {                                                                       \
        C3_SI_UNKNOWN, C3_OC_UNKNOWN, C3_EM_UNKNOWN, C3_ES_UNKNOWN, C3_OM_UNKNOWN, false \
    }
#define C3_PROFILE_NONE                                                                  \
    (C3_Profile) { C3_SI_NONE, C3_OC_NONE, C3_EM_NONE, C3_ES_NONE, C3_OM_NONE, false }
const static C3_Profile C3_PROFILE_NULL = C3_PROFILE_UNKNOWN;

/* Definition of specific profiles according to:
 * https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles#Profile_Definition
 */
const static C3_Profile C3_PROFILE_BASYSDEMO = (C3_Profile){
    C3_SI_OPERATIONS, C3_OC_NONE, C3_EM_NONE, C3_ES_BASYS, C3_OM_MULTI, false};
const static C3_Profile C3_PROFILE_BASYS =
    (C3_Profile){C3_SI_OPERATIONS,
                 (C3_OC_FACET)(C3_OC_OCCUPIED | C3_OC_PRIO),
                 (C3_EM_FACET)(C3_EM_AUTO | C3_EM_MANUAL),
                 C3_ES_BASYS,
                 C3_OM_MULTI,
                 false};
const static C3_Profile C3_PROFILE_PACKML = (C3_Profile){
    C3_SI_PACKML, C3_OC_NONE, (C3_EM_FACET)(C3_EM_AUTO | C3_EM_MANUAL | C3_EM_SEMIAUTO),
    C3_ES_PACKML, C3_OM_NONE, false};
const static C3_Profile C3_PROFILE_FULL =
    (C3_Profile){C3_SI_FULL, C3_OC_FULL, C3_EM_FULL, C3_ES_PACKML, C3_OM_PARALLEL, false};
const static C3_Profile C3_PROFILE_FULLOPTIONAL =
    (C3_Profile){C3_SI_FULL, C3_OC_FULL, C3_EM_FULL, C3_ES_PACKML, C3_OM_PARALLEL, true};

// TODO check how to initialize by constants above
const static C3_Profile C3_PROFILES[] = {
    (C3_Profile){C3_SI_OPERATIONS, C3_OC_NONE, C3_EM_NONE, C3_ES_BASYS, C3_OM_MULTI,
                 false},
    (C3_Profile){C3_SI_OPERATIONS, (C3_OC_FACET)(C3_OC_OCCUPIED | C3_OC_PRIO),
                 (C3_EM_FACET)(C3_EM_AUTO | C3_EM_MANUAL), C3_ES_BASYS, C3_OM_MULTI,
                 false},
    (C3_Profile){C3_SI_PACKML, C3_OC_NONE,
                 (C3_EM_FACET)(C3_EM_AUTO | C3_EM_MANUAL | C3_EM_SEMIAUTO), C3_ES_PACKML,
                 C3_OM_NONE, false},
    (C3_Profile){C3_SI_FULL, C3_OC_FULL, C3_EM_FULL, C3_ES_PACKML, C3_OM_PARALLEL, false},
    (C3_Profile){C3_SI_FULL, C3_OC_FULL, C3_EM_FULL, C3_ES_PACKML, C3_OM_PARALLEL, true}};
const static char *C3_PROFILES_STRINGS[] = {"BASYSDEMO", "BASYS", "PACKML", "FULL",
                                            "FULLOPTIONAL"};
const static int C3_PROFILES_COUNT = 5;

/*
 * Utility functions for profiles and facets
 */

// Get a facet value from a profile by FACET_GROUP enum
int
C3_Profile_toFacetValue(C3_Profile profile, FACET_GROUP facet);

// Convert a single facet value (eg. C3_SI_FACET_STRINGS) to a string from the
// corresponding array
const char *
C3_Profile_printFacet(const char **facets, int size, int value);

// Convert a profile value to a string (allocated on the heap)
char *
C3_Profile_print(C3_Profile p);

// Convert a string to a profile value.
// On error, a C3_PROFILE_NULL is returned.
// The string needs to comply with the syntax [SI,OC,EM,ES,OM,optional] or with a
// defined profile (BASYS, PACKML, FULL, etc.). In case of custom definition with syntax
// every facetgroup value (SI, OC, ...) needs to be specified as number, e.g.:
// [4,6,6,2,2,0] for BASYS profile.
// TODO add possibility to use String values for every facet group
// TODO change syntax to fit to C3_Profile_print output (SI:value,OC:value,...)
C3_Profile
C3_Profile_parse(char *string);

// Check 2 profiles for equality
bool
C3_Profile_isUnknown(const C3_Profile p);
bool
C3_Profile_isEqual(const C3_Profile p1, const C3_Profile p2);
bool
C3_Profile_isEqualOrUnknown(const C3_Profile p1, const C3_Profile p2);

#endif /* C3_PROFILES_H */