#ifndef C3_INFO_H
#define C3_INFO_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <C3_Profiles.h>

typedef struct {
    char *id;
    char *name;
    char *description;
    char *type;
    C3_Profile profile;
} C3_Info;

bool
C3_Info_init(C3_Info *info);

void
C3_Info_clear(C3_Info *info);

bool
C3_Info_copy(const C3_Info *src, C3_Info *dest);

#endif /* C3_INFO_H */