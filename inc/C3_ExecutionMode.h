#ifndef C3_EXECUTIONMODE_H
#define C3_EXECUTIONMODE_H

#include "C3_Result.h"
#include "C3_Profiles.h"

/*
 * Control component state machine for execution modes
 */
typedef enum {
    C3_EM_ORDER_AUTO,
    C3_EM_ORDER_SEMIAUTO,
    C3_EM_ORDER_MANUAL,
    C3_EM_ORDER_SIMULATE
} C3_EM_Order;
#define C3_EM_ORDERSIZE 4
static const char *C3_EM_ORDERSTRING[] = {"AUTO", "SEMIAUTO", "MANUAL", "SIMULATE"};

typedef enum {
    C3_EM_STATE_AUTO,
    C3_EM_STATE_SEMIAUTO,
    C3_EM_STATE_MANUAL,
    C3_EM_STATE_SIMULATE
} C3_EM_State;
#define C3_EM_STATESIZE 4
static const char *C3_EM_STATESTRING[] = {"AUTO", "SEMIAUTO", "MANUAL", "SIMULATE"};

C3_Result C3_EM_executeOrder(C3_EM_FACET facet, const C3_EM_Order order, const C3_EM_State state,
                               C3_EM_State* newState);

#endif /* C3_EXECUTIONMODE_H */