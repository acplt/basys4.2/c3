#ifndef C3_OPERATIONMODE_H
#define C3_OPERATIONMODE_H

#include "stdlib.h"
#include "stdbool.h"
#include "string.h"

#include "C3_ExecutionState.h"
#include "C3_InputOutput.h"

/* Forward definitions for control component and status as they include this file */
typedef struct C3_CC C3_CC;
typedef struct C3_Status C3_Status;

typedef struct C3_OP_OpMode{
    char* name;
    void* context;
    bool (*init)(C3_CC* cc, struct C3_OP_OpMode* opMode);
    void (*execute)(C3_CC* cc, struct C3_OP_OpMode* opMode,
                    C3_IO io, C3_ES_Order* order, const char** workingState, const char** errorState);
    void (*clear)(C3_CC* cc, struct C3_OP_OpMode* opMode);
    bool (*select)(C3_CC* cc, struct C3_OP_OpMode* opMode);
    bool (*deselect)(C3_CC* cc, struct C3_OP_OpMode* opMode);
} C3_OP_OpMode;

void C3_OP_OpMode_clear(C3_OP_OpMode* opMode);

bool C3_OP_OpMode_copy(const C3_OP_OpMode* src, C3_OP_OpMode* dest);

static const char* C3_OP_OPMODE_NONE = "NONE";
#define C3_OP_OPMODE_NONE_IDX -1

#endif /* C3_OPERATIONMODE_H */
