#ifndef C3_RESULT_H
#define C3_RESULT_H

#define C3_RESULT_OFFSET 12

typedef enum C3_Result {
    C3_RESULT_BAD_OPMODECHANGE = -C3_RESULT_OFFSET,
    C3_RESULT_BAD_OPMODENAME,
    C3_RESULT_BAD_OPMODEUNKNOWN,
    C3_RESULT_BAD_CMDSYNTAX,
    C3_RESULT_BAD_CMD,
    C3_RESULT_BAD_PROFILE,
    C3_RESULT_BAD_ACCESS,
    C3_RESULT_BAD_STATE,
    C3_RESULT_BAD_ORDER,
    C3_RESULT_BAD_NOTIMPLEMENTED,
    C3_RESULT_BAD_MEMORY,
    C3_RESULT_BAD,
    C3_RESULT_GOOD,
    C3_RESULT_GOOD_TARGETSTATEACTIVE
} C3_Result;

static const char *C3_RESULT_STRING[] = {"BadOpModeChange",
                                         "BadOpModeName",
                                         "BadOpModeUnknown",
                                         "BadCommandSyntax",
                                         "BadCommand",
                                         "BadAccess",
                                         "BadProfile",
                                         "BadState",
                                         "BadOrder",
                                         "BadNotImplemented",
                                         "BadMemory",
                                         "Bad",
                                         "Good",
                                         "GoodTargetStateActive"};

#define C3_RESULT_TOSTRING(result) C3_RESULT_STRING[result + C3_RESULT_OFFSET]

#endif /* C3_RESULT_H */