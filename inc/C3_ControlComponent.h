#ifndef C3_CONTROLCOMPONENT_H
#define C3_CONTROLCOMPONENT_H

#include "C3_Command.h"
#include "C3_Info.h"
#include "C3_InputOutput.h"

#include <limits.h>

/* Control Component (CC) definition and functions */
typedef struct C3_CC C3_CC;

/* Control component lifecycle */
C3_CC* C3_CC_new(void);

bool C3_CC_init(C3_CC* cc);

void C3_CC_clear(C3_CC* cc);

/* Get infos and status */
bool C3_CC_setInfo(C3_CC* cc, C3_Info* info);

const C3_Info C3_CC_getInfo(C3_CC* cc);

const char* C3_CC_getId(C3_CC* cc);

const C3_Status C3_CC_getStatus(C3_CC* cc);

char* C3_CC_printStatus(C3_CC* cc); //TODO change to print and print all infos for CC

/* Overwrite input output config */
void C3_CC_setIOConfig(C3_CC* cc, C3_IOConfig ioConfig);

/* Operation mode handling */
/* Add a new operation mode to the control component.
 * The opMode content will be copied to the heap, except for the context. */
C3_Result C3_CC_addOperationMode(C3_CC* cc, C3_OP_OpMode* opMode);
C3_Result C3_CC_deleteOperationMode(C3_CC* cc, const char* name);
/* Returns the count of operation modes and their names.
 * Parameter names is allowed to be NULL, so that operation modes are only counted.
 * Result 0 indicates no operation modes or not enough space to allocate memory for names. */
size_t C3_CC_getOperationModeNames(C3_CC* cc, char*** names);

/* Code execution */
void C3_CC_runIterate(C3_CC* cc);

/* Command input */
C3_Result C3_CC_Cmd(C3_CC* cc, C3_Command* cmd);
C3_Result C3_CC_CmdString(C3_CC* cc, const char* cmd);

#endif /* C3_CONTROLCOMPONENT_H */